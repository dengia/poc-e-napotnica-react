const webpack = require('webpack')
const merge = require('webpack-merge')
const common = require('./webpack.common.js')

const configProd = merge(common, {
  mode: 'production'
  , devtool: 'source-map'
})

module.exports = configProd