const webpack = require('webpack')
const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')
const copyWebpackPlugin = require('copy-webpack-plugin')
const extractTextPlugin = require('extract-text-webpack-plugin')

const BUILD_DIR = path.resolve(__dirname, 'public')
const SRC_DIR = path.resolve(__dirname, 'src')

const config = {
  entry: SRC_DIR + '/index.js'
  , output: {
      path: BUILD_DIR
      , publicPath: '/'
      , filename: 'bundle.js'
  }
  , module: {
      rules: [
      {
        test: /\.js$/
        , include: SRC_DIR
        , use: ['babel-loader']
      }
      , {
        test: /\.css$/
        , include: SRC_DIR
        , use: extractTextPlugin.extract({
          fallback: 'style-loader'
          , use: 
          {
            loader: 'css-loader',
            options: {
              modules: true // default is false
              , sourceMap: true
              , importLoaders: 1
              , localIdentName: '[name]--[local]--[hash:base64:8]'
          }
        }})
      } 
      , {
        test: /\.(png|svg|jpg|gif)$/
        , use: ['file-loader']
      }
      , {
        test: /\.(woff|woff2|eot|ttf|otf)$/
        , use: ['file-loader']
      }      
    ]
  }  
  , plugins: [
    new htmlWebpackPlugin({
      title: 'poc-e-napotnica'
      , template: SRC_DIR + '/index_template.html'
      , filename: 'index.html'
      , inject: 'body'
    })
    , new copyWebpackPlugin([
        { from:'src/Images', to:'images' } 
    ])
    , new extractTextPlugin('bundle.css')
    ]  
}

module.exports = config