const webpack = require('webpack')
const merge = require('webpack-merge')
const common = require('./webpack.common.js')

const configDev = merge(common, {
  mode: 'development'
  , devtool: 'inline-source-map'
})

module.exports = configDev