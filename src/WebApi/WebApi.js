
export default class WebApi {
  constructor(props) {
    this.tissuesTemplate = []
    this.listTissueType = []
    this.listNrOfBits = []
    this.listFixative = []

    this.getTissueTemplateList = this.getTissueTemplateList.bind(this)
    this.getTissueTypeList = this.getTissueTypeList.bind(this)
    this.getLocationList = this.getLocationList.bind(this)
    this.getNrOfBitsList = this.getNrOfBitsList.bind(this)
    this.getFixativeList = this.getFixativeList.bind(this)
  }

  getTissueTemplateList() {
    if (this.tissuesTemplate.length === 0) {
      const proto = {
        isSelected: false
        , index: 0
        , caption: ''
        , tissueType: {
            value: ''
          , helperText: ''
          , error: false
        }
        , location: {
            value: ''
          , helperText: ''
          , error: false
        }
        , nrOfBits: {
            value: ''
          , helperText: ''
          , error: false
        }
        , casOdvzema: {
            value: ''
          , helperText: ''
          , error: false
        }
        , fixative: {
            value: ''
          , helperText: ''
          , error: false
        }
        , nacinOdvzema: {
            value: ''
          , helperText: ''
          , error: false
        }
        , opomba: {
            value: ''
          , helperText: ''
          , error: false
        }          
      }

      for (let i = 0; i < 25; i++) {
        //DG: Not the best option for cloning. But is quick and short
        const element = JSON.parse(JSON.stringify(proto))
        element.index = i
        element.caption = String.fromCharCode(65 + i)
        this.tissuesTemplate.push(element)
      }
    }
    return this.tissuesTemplate
  }

  getTissueTypeList() {
    if (this.listTissueType.length === 0) {
      //TODO: Retreive this from real WebApi
      this.listTissueType.push(
        { key:'1', value:'1', label:'KozaBio'}
        , { key:'2', value:'2', label:'KozaEks'}
      )
    }
    return this.listTissueType
  }
  getLocationList(tissueType) {
    let list = []
    switch (tissueType) {
      case 1:
        list.push(
          { key:'1', value:'1', label:'A'}
          , { key:'2', value:'2', label:'B'}
        )        
        break;
      case 2:
        list.push(
          { key:'1', value:'1', label:'AA'}
          , { key:'2', value:'2', label:'BB'}
        )        
        break;
      case 3:
        list.push(
          { key:'1', value:'1', label:'AAA'}
          , { key:'2', value:'2', label:'BBB'}
        )        
        break;           
      default:
        break;
    }

    return list
  }
  getNrOfBitsList() {
    if (this.listNrOfBits.length === 0) {
      this.listNrOfBits.push(
        { key:'1', value:'1', label:'1'}
        , { key:'2', value:'2', label:'2'}
        , { key:'3', value:'3', label:'3'}
        , { key:'4', value:'4', label:'4'}
        , { key:'5', value:'5', label:'5'}
        , { key:'6', value:'6', label:'>5'}
      )
    }
    return this.listNrOfBits   
  }
  getFixativeList() {
    if (this.listFixative.length === 0) {
      this.listFixative.push(
        { key:'1', value:'1', label:'Nativno'}
        , { key:'2', value:'2', label:'10%'}
        , { key:'3', value:'3', label:'Fiksativ ?'}
      )
    }
    return this.listFixative   
  }        
}