import WebApi from './WebApi'

class WebApiMock extends WebApi {
  constructor(props) {
    super(props)
  }

  getTissueTemplateList() {
    if (this.tissuesTemplate.length === 0) {
      const proto = {
        isSelected: false
        , index: 0
        , caption: ''
        , tissueType: {
            value: ''
          , helperText: ''
          , error: false
        }
        , location: {
            value: ''
          , helperText: ''
          , error: false
        }
        , nrOfBits: {
            value: ''
          , helperText: ''
          , error: false
        }
        , casOdvzema: {
            value: ''
          , helperText: ''
          , error: false
        }
        , fixative: {
            value: ''
          , helperText: ''
          , error: false
        }
        , nacinOdvzema: {
            value: ''
          , helperText: ''
          , error: false
        }
        , opomba: {
            value: ''
          , helperText: ''
          , error: false
        }          
      }

      for (let i = 0; i < 25; i++) {
        //DG: Not the best option for cloning. But is quick and short
        const element = JSON.parse(JSON.stringify(proto))
        element.index = i
        element.caption = String.fromCharCode(65 + i)
        this.tissuesTemplate.push(element)
      }
    }
    return this.tissuesTemplate
  }

  getTissueTypeList() {
    if (this.listTissueType.length === 0) {
      this.listTissueType.push(
        { key:'1', value:'1', label:'BezgATI'}
        , { key:'2', value:'2', label:'BezgBio'}
        , { key:'3', value:'3', label:'DojkATI'}
        , { key:'4', value:'4', label:'DojkBio'}
        , { key:'5', value:'5', label:'DvanBio'}
        , { key:'6', value:'6', label:'DvanEks'}
        , { key:'7', value:'7', label:'HipoATI'}
        , { key:'8', value:'8', label:'HipoBio'}
        , { key:'9', value:'9', label:'JetrATI'}
        , { key:'10', value:'10', label:'JetrEks'}
        , { key:'11', value:'11', label:'KoloATI'}
        , { key:'12', value:'12', label:'KoloBio'}
        , { key:'13', value:'13', label:'KostATI'}
        , { key:'14', value:'14', label:'KostBio'}
        , { key:'15', value:'15', label:'LedvATI'}
        , { key:'16', value:'16', label:'LedvEks'}
      )
    }
    return this.listTissueType   
  }
  getLocationList(tissueType) {
    let list = []
    switch (tissueType) {
      case '1':
        list.push(
          { key:'1', value:'1', label:'A'}
          , { key:'2', value:'2', label:'B'}
        )        
        break;
      case '2':
        list.push(
          { key:'1', value:'1', label:'AA'}
          , { key:'2', value:'2', label:'BB'}
        )        
        break;
      case '3':
        list.push(
          { key:'1', value:'1', label:'AAA'}
          , { key:'2', value:'2', label:'BBB'}
        )        
        break;
      case '4':
        list.push(
          { key:'1', value:'1', label:'AAAA'}
          , { key:'2', value:'2', label:'BBBB'}
        )        
        break;
      case '5':
        list.push(
          { key:'1', value:'1', label:'AAAAA'}
          , { key:'2', value:'2', label:'BBBBB'}
        )        
        break;
      case '6':
        list.push(
          { key:'1', value:'1', label:'AAAAAA'}
          , { key:'2', value:'2', label:'BBBBBB'}
        )        
        break;                                                                 
      default:
        list.push(
          { key:'1', value:'1', label:'AAA?'}
          , { key:'2', value:'2', label:'BBB?'}
        )                        
        break;
    }

    return list
  }
  getNrOfBitsList() {
    if (this.listNrOfBits.length === 0) {
      this.listNrOfBits.push(
        { key:'1', value:'1', label:'1'}
        , { key:'2', value:'2', label:'2'}
        , { key:'3', value:'3', label:'3'}
        , { key:'4', value:'4', label:'4'}
        , { key:'5', value:'5', label:'5'}
        , { key:'6', value:'6', label:'>5'}
      )
    }
    return this.listNrOfBits   
  }
  getFixativeList() {
    if (this.listFixative.length === 0) {
      this.listFixative.push(
        { key:'1', value:'1', label:'Nativno'}
        , { key:'2', value:'2', label:'10%'}
        , { key:'3', value:'3', label:'Fiksativ ?'}
      )
    }
    return this.listFixative   
  }    
}

export default WebApiMock