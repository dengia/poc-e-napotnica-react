import WebApi from './WebApi'
import WebApiMock from './WebApiMock'

class WebApiFactory {
  static Instance(type) {
    if (type === 'mock')
      return new WebApiMock()
    else
      return new WebApi()
  }
}

const webApi = WebApiFactory.Instance('mock')
Object.freeze(webApi)

export default webApi