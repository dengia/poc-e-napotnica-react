class EnrichedDevice {
  constructor(name, column, columnSpan) {
    this.name = name
    this.column = column
    this.columnSpan = columnSpan
      if (this.name === EnrichedDevice.DesktopDevice)
        this.colIdentifier = '-'
      else if (this.name === EnrichedDevice.TabletDevice)
        this.colIdentifier = '-m-'
      else if (this.name === EnrichedDevice.PhoneDevice)
        this.colIdentifier = '-p-'
      else
        this.colIdentifier = '-'    
  }
  get isMissing() {
    return this.column === -1
  }
  get isNotEnriched() {
    return this.column === -2
  }

  static get DesktopDevice() {
    return 'Desktop'
  }
  static get TabletDevice() {
    return 'Tablet'
  }
  static get PhoneDevice() {
    return 'Phone'
  }
  static Create(device) {
    return new EnrichedDevice(device.type.name, device.props.column, device.props.columnSpan)
  }  
  static CreateMissing(name) {
    return new EnrichedDevice(name, -1, 0)
  }
  static CreateNotEnriched(name) {
    return new EnrichedDevice(name, -2, 0)
  }
  static CreateMissingDesktop() {
    return new EnrichedDevice(this.DesktopDevice, -1, 0)
  }  
  static CreateMissingTablet() {
    return new EnrichedDevice(this.TabletDevice, -1, 0)
  }
  static CreateMissingPhone() {
    return new EnrichedDevice(this.PhoneDevice, -1, 0)
  }
  static GetInitalList() {
    const list = [EnrichedDevice.CreateMissingDesktop(), EnrichedDevice.CreateMissingTablet(), EnrichedDevice.CreateMissingPhone()]
    return list
  }
  static GetNotEnrichedList() {
    const list = [EnrichedDevice.CreateNotEnriched('Desktop'), EnrichedDevice.CreateNotEnriched('Tablet'), EnrichedDevice.CreateNotEnriched('Phone')]
    return list
  }
  static GetByName(enrichedDevices, name) {
    if (enrichedDevices === undefined)
      return undefined
    for (let i = 0; i < enrichedDevices.length; i++) {
      if (enrichedDevices[i].name === name)
        return enrichedDevices[i]
    }
    return EnrichedDevice.CreateMissing(name)
  }
  Enrich(device) {
    this.column = device.props.column
    this.columnSpan = device.props.columnSpan
    return this
  }
  GetDivClassName() {
    console.log('EnrichedDevice.GetDivClassName: ', this.isMissing, this.colIdentifier, this.columnSpan)
    if (this.isMissing)
      return 'zero' + this.colIdentifier
    return 'col' + this.colIdentifier + this.columnSpan
  }  
  GetVirtualDivClassName(preEnrichedDevice) {
    console.log('EnrichedDevice.GetVirtualDivClassName: ', this.isMissing, this.colIdentifier, this.columnSpan)
    if (this.isMissing)
      return 'zero' + this.colIdentifier

    let pre = preEnrichedDevice
    if (pre !== undefined 
      && ((parseInt(pre.column, 10) + parseInt(pre.columnSpan, 10) === 12)
            || parseInt(pre.column, 10) + parseInt(pre.columnSpan, 10) > parseInt(this.column, 10)))
      pre = undefined

    let virtualSpan = 0
    if (pre === undefined) { //This is first device
      if (parseInt(this.column, 10) > 0)
        virtualSpan = parseInt(this.column, 10)
    }
    else {
      if (parseInt(this.column, 10) > 0)
        virtualSpan = parseInt(this.column, 10) - (parseInt(pre.column, 10) + parseInt(pre.columnSpan, 10))
    }
    if (virtualSpan === 0)
      return 'zero' + this.colIdentifier
    else if (virtualSpan < 0)
      console.log('virtualSpan is intended to be >= 0 however in your case virtualSpan equals ' + virtualSpan + '. Check your layout definition for device ' + this.name)

    console.log('col' + this.colIdentifier + virtualSpan)

    return 'col' + this.colIdentifier + virtualSpan
  } 
}

export default EnrichedDevice