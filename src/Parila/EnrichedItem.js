import React, {Component} from 'react'
import EnrichedDevice from './EnrichedDevice'
import Device from './Devices/Device.js'
import Layout from './Layout.js'

class EnrichedItem {
  constructor(item, className, devices){
    this.item = item
    this.className = className
    this.devices = devices
  }

  get isUndefined(){
    if (this.className === undefined)
      return true

    return false
  }

  static Create(item, className, enrichedDevices) {
    return new EnrichedItem(item, className, enrichedDevices)
  }  
  static CreateUndefined() {
    return new EnrichedItem(undefined, undefined, undefined)
  }
  static CreateNotEnriched(item) {
    return new EnrichedItem(item, undefined, EnrichedDevice.GetNotEnrichedList())
  }
  static Enrich(item) { //item represents react component in Grid
    const initialDevicesAll = EnrichedDevice.GetInitalList()
    for (let i = 0; i < React.Children.count(item.props.children); i++) {
      const itemChild = Array.isArray(item.props.children) ? item.props.children[i] : item.props.children;
      if (itemChild !== undefined && itemChild.type !== undefined && itemChild.type.name === 'Layout') {
        const layout = itemChild
        let className = ''
        let enrichedDevicesAll = []
        const desiredDevices = Layout.GetDevices(layout)
        for (let j = 0; j < initialDevicesAll.length; j++) {
          const initialDevice = initialDevicesAll[j]
          const device = Device.GetByName(desiredDevices, initialDevice.name)
          const enrichedDevice = initialDevice.Enrich(device)
          className += enrichedDevice.GetDivClassName() + ' '
          enrichedDevicesAll.push(enrichedDevice)
        }
        className = className.trim()
        return EnrichedItem.Create(item, className, enrichedDevicesAll)
      }
    }
    return EnrichedItem.CreateNotEnriched(item)
  }  
}

export default EnrichedItem