import React, {Component} from 'react'

class Device extends Component {
  render() {
    return null //Device is invisible
  }
  static GetByName(devices, name) {
    for (let i = 0; i < devices.length; i++) {
      const device = devices[i]
      if (device.type.name === name)
        return device
    }
    //If we got so far then device is not defined hence undefined :-)
  }  
}

Device.defaultProps = {
    column: "0"
    , columnSpan: "12"
}

export default Device