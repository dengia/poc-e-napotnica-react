import React, {Component} from 'react'

//Layout should be one child of your Component
class Layout extends React.Component {
  render() {
    return null //Layout is invisible :-)
  }
  static GetDevices(layout) {
    const list = []
    React.Children.map(layout.props.children, (item, index) => {
      //TODO: check this item is of type Device!
      list.push(item)
    })
    return list
  }  
}

export default Layout