import React, {Component} from 'react'
import EnrichedItem from './EnrichedItem'
import EnrichedDevice from './EnrichedDevice'
import GridStyle from './Grid.css'
import Layout from './Layout'

//Gird contains your components and is responsible to set class for each one
//based on Layout settings within your component
class Grid extends React.Component {

  render() {
    console.log('grid.render')

    let enrichedItems = []
    React.Children.map(this.props.children, (item, index) => {
      enrichedItems.push(EnrichedItem.Enrich(item))
    })

    const initialDevicesAll = EnrichedDevice.GetInitalList()
    return <React.Fragment>{this.RenderChildren(enrichedItems, initialDevicesAll)}</React.Fragment>
  }

  RenderChildren(enrichedItems, initialDevicesAll) {
  
    let result = []
    let preEnrichedItem = EnrichedItem.CreateUndefined()

    for (let i = 0; i < enrichedItems.length; i++) {
      const enrichedItem = enrichedItems[i]  
      result.push(this.RenderItem(preEnrichedItem, enrichedItem, initialDevicesAll))
      preEnrichedItem = enrichedItem
    }

    return result
  }

  RenderItem(preEnrichedItem, enrichedItem, initialDevicesAll) {

    console.log('grid.RenderItem: ', enrichedItem)

    let virtualDivClassName = ''
    for (let i = 0; i < initialDevicesAll.length; i++) {
      const deviceName = initialDevicesAll[i].name
      const preEnrichedDevice = EnrichedDevice.GetByName(preEnrichedItem.devices, deviceName)
      const enrichedDevice = EnrichedDevice.GetByName(enrichedItem.devices, deviceName)
      virtualDivClassName += enrichedDevice.GetVirtualDivClassName(preEnrichedDevice) + ' '
    }
    virtualDivClassName = virtualDivClassName.trim()

    console.log('grid.RenderItem.virtualDivClassName: ', virtualDivClassName)
    console.log('grid.RenderItem.enrichedItem.className: ', enrichedItem.className)

    if (this.ShouldRenderDiv(enrichedItem.className)){
      if (this.ShouldRenderDiv(virtualDivClassName)) {
        return <React.Fragment>
                 <div className = {virtualDivClassName + ' ' + GridStyle.virtualStyle}></div>
                 <div className = {enrichedItem.className + ' ' + GridStyle.actualStyle}>{enrichedItem.item}</div>
                </React.Fragment>
      }
      else
        return <div className = {enrichedItem.className + ' ' + GridStyle.actualStyle}>{enrichedItem.item}</div>  
    }
  }  
  
  ShouldRenderDiv(className) {
    if(className === undefined)
      return true
    const cn = className.trim()
    return !(cn === 'zero-' || cn === 'zero- zero-m-' || cn === 'zero- zero-m- zero-p-')
  }
}

export default Grid