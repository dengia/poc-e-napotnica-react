import React from 'react'

//My components
import LocationGridRow from './LocationGridRow'

//Style
import style from './LocationGrid.css'

class LocationGrid extends React.Component {
  constructor(props) {
    super(props)

    this.handleClick = this.handleClick.bind(this)

    this.state = {
      rows: this.getRows()
    }

  }

  getRows() {
    let rows = []
    for (let i = 0; i < this.props.rows; i++) {
      let row = { row: i, className: style.row, cells: [] }
      for (let j = 0; j < this.props.columns; j++) {
        row.cells.push( { row: i, column: j, className: style.cell + ' ' + style.unselected, onClickHandler: this.handleClick, isSelected: false } )
      }
      rows.push(row)
    }
    return rows
  }

  handleClick(row, column) {
    const rows = this.state.rows.slice()
    const cell = rows[row].cells[column]
    cell.isSelected = !cell.isSelected
    cell.className = cell.isSelected ? style.cell + ' ' + style.selected : style.cell + ' ' + style.unselected
    this.setState({...this.state, rows: rows})
  }

  render() {
    return(
      <table className={this.props.style.grid}>
        <tbody>
        {
          this.state.rows.map((row, index) => { 
            return <LocationGridRow key={row.row} row={row.row} className={row.className} cells={row.cells}/>
          })
        }
        </tbody>
      </table>
    )
  }
}

export default LocationGrid