import React from 'react'

//My components
import LocationGridCell from './LocationGridCell'

class LocationGridRow extends React.Component {
  render() {
    return(
      <tr className={this.props.className}>
        {
          this.props.cells.map((cell, index) => {
            return <LocationGridCell key={cell.row + '-' + cell.column} row={cell.row} column={cell.column} className={cell.className} onClickHandler={cell.onClickHandler} />
          })
        }
      </tr>
    )
  }
}

export default LocationGridRow