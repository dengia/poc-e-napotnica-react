import React from 'react'
import { Link } from 'react-router-dom'

//Material-ui
import Button from '@material-ui/core/Button'

//Parila
import Grid from '../Parila/Grid'
import Layout from '../Parila/Layout'
import Desktop from '../Parila/Devices/Desktop'
import Tablet from '../Parila/Devices/Tablet'
import Phone from '../Parila/Devices/Phone'

//My components
import InvisibleDiv from './InvisibleDiv'

//Style
import style from './OrderFooter.css'

class OrderFooter extends React.Component {
  render() {
    return(
      <div className={style.orderFooter}>
        <Grid>       
          <Link to='/order-print' className={style.linkPrint}>
            <Button size="small" color="secondary" className={style.buttonPrint}>
              {this.props.orderPrintButtonLabel}              
            </Button>
            <Layout>
              <Desktop column="0" columnSpan="4"/>
              <Tablet column="0" columnSpan="4"/>
              <Phone/>
            </Layout>            
          </Link>
          <Link to='/' className={style.linkOrderList}>
            <Button size="small" color="secondary" className={style.buttonOrderList}>
              {this.props.orderOrderListButtonLabel}              
            </Button>
            <Layout>
              <Desktop column="4" columnSpan="4"/>
              <Tablet column="4" columnSpan="4"/>
              <Phone/>
            </Layout>            
          </Link>          
          <Link to='/order-print' className={style.linkSubmit}>
            <Button size="small" color="primary" className={style.buttonSubmit}>
              {this.props.orderSubmitButtonLabel}              
            </Button>
            <Layout>
              <Desktop column="8" columnSpan="4"/>
              <Tablet column="8" columnSpan="4"/>
              <Phone/>
            </Layout>            
          </Link>          
          
        </Grid>
      </div>
    )
  }
}

export default OrderFooter