import React from 'react'

class LocationGridCell extends React.Component {
  constructor(props) {
    super(props)

    this.onClickHandler = this.onClickHandler.bind(this)
  }
  
  onClickHandler() {
    this.props.onClickHandler(this.props.row, this.props.column)
  }

  render() {
    return(
      <td className={this.props.className} onClick={this.onClickHandler} hover={()=>console.log('hover')}>
      </td>
    )
  }
}

export default LocationGridCell