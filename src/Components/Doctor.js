import React from 'react'

//Material UI
import TextField from '@material-ui/core/TextField';

//Parila
import Grid from '../Parila/Grid'
import Layout from '../Parila/Layout'
import Desktop from '../Parila/Devices/Desktop'
import Tablet from '../Parila/Devices/Tablet'
import Phone from '../Parila/Devices/Phone'

class Doctor extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      uniquifier: ''
      , name: ''
      , surname: ''
    }

    this.handleChangeUniquifier = this.handleChangeUniquifier.bind(this)
  }

  handleChangeUniquifier(e) {
    const value = e.target.value
    if (value === '12345') {
      this.setState(...this.state, { 
          uniquifier: value 
          , name: 'Domen'
          , surname: 'Vrhovnik'
        })
    } else if (value === '54321') {
      this.setState(...this.state, { 
          uniquifier: value 
          , name: 'Milojka'
          , surname: 'Kolar'
        })
    } else {
      this.setState(...this.state, { 
        uniquifier: value 
        , name: ''
        , surname: ''
      })
    }
  }

  render() {
    //console.log(this.props)
    //console.log(this.props.style)
    return(
      <div className={this.props.style.divDoctor}>
      <Grid>
        <TextField
            id="doctor-uniquifier"
            label={this.props.uniquifierLabel}
            helperText={this.props.uniquifierHelperText}
            error={this.props.uniquifierError}           
            className={this.props.style.uniquifier}
            value={this.state.uniquifier}
            onChange={this.handleChangeUniquifier}
            disabled={false}
        >
          <Layout>
            <Desktop column="0" columnSpan="3"/>
            <Tablet column="0" columnSpan="3"/>
            <Phone/>
          </Layout> 
        </TextField>
        <TextField
            id="doctor-name"
            label={this.props.nameLabel}         
            className={this.props.style.name}
            value={this.state.name}
            disabled={false}
        >
          <Layout>
            <Desktop column="3" columnSpan="3"/>
            <Tablet column="3" columnSpan="3"/>
            <Phone/>
          </Layout>
        </TextField>
        <TextField
            id="doctor-surname"
            label={this.props.surnameLabel}         
            className={this.props.style.surname}
            value={this.state.surname}
            disabled={false}
        >
          <Layout>
            <Desktop column="6" columnSpan="6"/>
            <Tablet column="6" columnSpan="6"/>
            <Phone/>
          </Layout>
        </TextField>       
      </Grid>
      </div>      
    )
  }
}

export default Doctor