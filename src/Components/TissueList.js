import React from 'react'
import Tissue from './Tissue'

class TissueList extends React.Component {
  constructor(props) {
    super(props)

  }

  render() {
    return(
      <React.Fragment>
          {this.props.tissues.map((tissue, i) => {
            return (
              <Tissue key={i} tissue={tissue}>
              </Tissue>
            );
          })}
      </React.Fragment>
    )
  }
}

export default TissueList