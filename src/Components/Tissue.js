import React from 'react'

//My components
import webApi from '../WebApi/WebApiFactory'
import Location from './Location'

//Material-UI
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Badge from '@material-ui/core/Badge';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Icon from '@material-ui/core/Icon';

//Style
import style from './Tissue.css'

class Tissue extends React.Component {
  constructor(props) {
    super(props)
    this.listTissueType = webApi.getTissueTypeList()
    this.listLocation = []
    this.listNrOfBits = webApi.getNrOfBitsList()
    this.listFixative = webApi.getFixativeList()

    this.state = {
      tissue: this.props.tissue
      , locationIsOpen: false
      , opombaIsOpen: false   
    }

    this.handleTissueTypeOnChange = this.handleTissueTypeOnChange.bind(this)
    this.handleLocationOnChange = this.handleLocationOnChange.bind(this)
    this.handleNrOfBitsOnChange = this.handleNrOfBitsOnChange.bind(this)
    this.handleCasOdvzemaOnChange = this.handleCasOdvzemaOnChange.bind(this)
    this.handleFixativeOnChange = this.handleFixativeOnChange.bind(this)
    this.openOpomba = this.openOpomba.bind(this)
    this.closeOpomba = this.closeOpomba.bind(this)
    this.handleOpombaDialogOnClose = this.handleOpombaDialogOnClose.bind(this)
    this.handleOpombaOnChange = this.handleOpombaOnChange.bind(this)
    this.openLocation = this.openLocation.bind(this)
    this.closeLocation = this.closeLocation.bind(this)
    this.handleLocationDialogOnClose = this.handleLocationDialogOnClose.bind(this)

  }

  handleTissueTypeOnChange(e) {
    this.listLocation = webApi.getLocationList(e.target.value)
    const isSelected = e.target.value != undefined     
    this.setState({...this.state, tissue: {...this.state.tissue, isSelected: isSelected, tissueType: { value: e.target.value }}})
  }

  handleLocationOnChange(e) {
    this.setState({...this.state, tissue: {...this.state.tissue, location: { value: e.target.value }}})
  }

  handleNrOfBitsOnChange(e) {
    this.setState({...this.state, tissue: {...this.state.tissue, nrOfBits: { value: e.target.value }}})
  }

  handleCasOdvzemaOnChange(e) {
    this.setState({...this.state, tissue: {...this.state.tissue, casOdvzema: { value: e.target.value }}})
  }

  handleFixativeOnChange(e) {
    this.setState({...this.state, tissue: {...this.state.tissue, fixative: { value: e.target.value }}})
  }

  handleOpombaOnChange(e) {
    this.setState({...this.state, tissue: {...this.state.tissue, opomba: { value: e.target.value }}})
  }

  openOpomba() {
    this.setState({...this.state, opombaIsOpen: true })
  }

  closeOpomba() {
    this.setState({...this.state, opombaIsOpen: false })
  }

  handleOpombaDialogOnClose() {

  }

  openLocation() {
    this.setState({...this.state, locationIsOpen: true })
  }

  closeLocation() {
    this.setState({...this.state, locationIsOpen: false })
  }

  handleLocationDialogOnClose() {

  }

  copy() {

  }

  render() {
    return(
      <div className={this.state.tissue.isSelected ? style.tissue + ' ' + style.selected : style.tissue + ' ' + style.unselected}>
        <span className={style.index}>{this.state.tissue.caption}</span>
        <IconButton color="primary" component="span" onClick={this.copy}>
          <Icon>play_arrow</Icon>
        </IconButton>
        <TextField
          label='Vrsta'
          value={this.state.tissue.tissueType.value}
          onChange={this.handleTissueTypeOnChange}
          helperText={this.state.tissue.tissueType.helperText}
          error={this.state.tissue.tissueType.error}
          className={style.tissueType}
          select
          SelectProps={{
            MenuProps: {
              className: style.tissueTypeItem
            }
          }}
        >
            {this.listTissueType.map(item => (
              <MenuItem key={item.key} value={item.value}>
                {item.label}
              </MenuItem>
            ))}        
        </TextField>
        <Button size="small" color="primary" className={style.buttonLocation} onClick={this.openLocation}>
          Lokacija              
        </Button>
        <TextField
          label='Št. koščkov'
          value={this.state.tissue.nrOfBits.value}
          onChange={this.handleNrOfBitsOnChange}
          helperText={this.state.tissue.nrOfBits.helperText}
          error={this.state.tissue.nrOfBits.error}
          className={style.nrOfBits}
          select
          SelectProps={{
            MenuProps: {
              className: style.nrOfBitsItem
            }
          }}
        >
            {this.listNrOfBits.map(item => (
              <MenuItem key={item.key} value={item.value}>
                {item.label}
              </MenuItem>
            ))}        
        </TextField>
        <TextField
          label='Čas odvzema'
          value={this.state.tissue.casOdvzema.value}
          onChange={this.handleCasOdvzemaOnChange}
          helperText={this.state.tissue.casOdvzema.helperText}
          error={this.state.tissue.casOdvzema.error}
          className={style.casOdvzema}
        >       
        </TextField>
        <TextField
          label='Fiksativ'
          value={this.state.tissue.fixative.value}
          onChange={this.handleFixativeOnChange}
          helperText={this.state.tissue.fixative.helperText}
          error={this.state.tissue.fixative.error}
          className={style.fixative}
          select
          SelectProps={{
            MenuProps: {
              className: style.fixativeItem
            }
          }}
        >
            {this.listFixative.map(item => (
              <MenuItem key={item.key} value={item.value}>
                {item.label}
              </MenuItem>
            ))}        
        </TextField>     
        <Button size="small" color="primary" className={style.buttonNotes} onClick={this.openOpomba}>
            Opomba
        </Button>

        <Dialog
          open={this.state.locationIsOpen}
          onClose={this.handleLocationDialogOnClose}
          aria-labelledby="locationDialogTitle"
        >
          <DialogTitle id="locationDialogTitle">{'Lokacija odvzema za vzorec ' + this.state.tissue.caption}</DialogTitle>
          <DialogContent>
            <Location index={this.state.tissue.index} caption={this.state.tissue.caption} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeLocation} color="secondary">
              Prekliči
            </Button>             
            <Button onClick={this.closeLocation} color="primary">
              V redu
            </Button>           
          </DialogActions>
        </Dialog> 

        <Dialog
          open={this.state.opombaIsOpen}
          onClose={this.handleOpombaDialogOnClose}
          aria-labelledby="opombaDialogTitle"
        >
          <DialogTitle id="opombaDialogTitle">{'Vzorec ' + this.state.tissue.caption}</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              value={this.state.tissue.opomba.value}
              onChange={this.handleOpombaOnChange}              
              id="name"
              label="Opomba"
              fullWidth
              multiline={true}
            />
          </DialogContent>
            
          <DialogActions>
            <Button onClick={this.closeOpomba} color="secondary">
              Prekliči
            </Button>              
            <Button onClick={this.closeOpomba} color="primary">
              V redu
            </Button>          
          </DialogActions>
        </Dialog>                   
      </div>
    )
  }
}

export default Tissue