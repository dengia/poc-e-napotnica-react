import React from 'react'

//Material UI
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

//Style
import style from './OrderList.css'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

let id = 0;
function createData(preiskava, poslano, status, statusLastChanged, prejeto, pacient, napotniZdravnik) {
  id += 1;
  return { id, preiskava, poslano, status, statusLastChanged, prejeto, pacient, napotniZdravnik };
}

const data = [
  createData('18-H-00001', '04.01.2018', 'Zaključeno', '06.01.2018 10:33', '06.01.2018 14:21', 'Luka Gomilšek', 'Lan Grgov'),
  createData('18-H-00008', '05.01.2018', 'Zaključeno', '06.01.2018 09:57', '06.01.2018 12:03', 'Jasna Lukančič-Srebrnič', 'Lan Grgov'),
  createData('18-L-00001', '04.01.2018', 'Zaključeno', '04.01.2018 16:47', '04.01.2018 18:57', 'Sebastijan Henigman', 'Romana Krkovčič'),
  createData('18-H-00783', '14.04.2018', 'Zaključeno', '14.04.2018 11:15', '14.04.2018 13:21', 'Romina Novak', 'Domen Domadenik'),
  createData('18-H-00803', '26.04.2018', 'Zaključeno', '30.04.2018 12:12', '30.04.2018 14:15', 'Nina Katarina Markon', 'Herman Volavšek'),
];

class OrderList extends React.Component {
  render() {
    return(
      <Paper className={style.root}>
      <Table className={style.table}>
        <TableHead>
          <TableRow>
            <TableCell>Preiskava</TableCell>
            <TableCell>Poslano</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Zadnja sprememba</TableCell>
            <TableCell>Prejeto</TableCell>
            <TableCell>Pacient</TableCell>
            <TableCell>Napotni zdravnik</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(n => {
            return (
              <TableRow key={n.id}>
                <TableCell>
                  {n.preiskava}
                </TableCell>
                <TableCell>{n.poslano}</TableCell>
                <TableCell>{n.status}</TableCell>
                <TableCell>{n.statusLastChanged}</TableCell>
                <TableCell>{n.prejeto}</TableCell>
                <TableCell>{n.pacient}</TableCell>
                <TableCell>{n.napotniZdravnik}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
    )
  }
}

export default OrderList