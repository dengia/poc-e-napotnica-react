import React from 'react'
import { Link } from 'react-router-dom'

//Material-ui
import Button from '@material-ui/core/Button'

//Parila
import Grid from '../Parila/Grid'
import Layout from '../Parila/Layout'
import Desktop from '../Parila/Devices/Desktop'
import Tablet from '../Parila/Devices/Tablet'
import Phone from '../Parila/Devices/Phone'

//My components
import InvisibleDiv from './InvisibleDiv'

//Style
import style from './OrderListFooter.css'

class OrderListFooter extends React.Component {
  render() {
    return(
      <div className={style.orderListFooter}>
        <Grid>       
          <Link to='/order' className={style.linkNewOrder}>
            <Button size="small" color="secondary" className={style.buttonNewOrder}>
              {this.props.orderListNewOrderButtonLabel}              
            </Button>
            <Layout>
              <Desktop column="8" columnSpan="4"/>
              <Tablet column="8" columnSpan="4"/>
              <Phone/>
            </Layout>            
          </Link>
        </Grid>
      </div>
    )
  }
}

export default OrderListFooter