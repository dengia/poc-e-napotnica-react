import React from 'react'

//Material UI
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class Customer extends React.Component {
  render() {
    return(
      <div>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>Naročnik: Klinični oddelek za urologijo</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
            Univerzitetni klinični center Ljubljana
            Klinični oddelek za urologijo
            Njegoševa 49, 1000 Ljubljana
            Telefon: 01 789 54 23

            ID DDV: SI45687123
            TRR: SI56 0123 4567 8901 1234
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      </div>
    )
  }
}

export default Customer