import React from 'react'

//My components
import LocationGrid from './LocationGrid'

//Style
import style from './Location.css'

class Location extends React.Component {
  render() {
    return(
      <div className={style.imageDiv}>
        <img src={'images/TissueType_' + this.props.index + '.png'} alt="Označite lokacijo odvzema"/>
        <LocationGrid style={style} rows={15} columns={10} />
      </div>
    )
  }
}

export default Location