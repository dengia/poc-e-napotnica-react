import React from 'react'

import style from './InvisibleDiv.css'

export default class InvisibleDiv extends React.Component {
  render() {
    return (
      <div className={style.invisibleDiv}>
      </div>
    )
  }
}