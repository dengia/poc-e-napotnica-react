import React from 'react'

//Material UI
import TextField from '@material-ui/core/TextField';

//Parila
import Grid from '../Parila/Grid'
import Layout from '../Parila/Layout'
import Desktop from '../Parila/Devices/Desktop'
import Tablet from '../Parila/Devices/Tablet'
import Phone from '../Parila/Devices/Phone'

//Style
import style from './Patient.css'

class Patient extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      uniquifier: ''
      , name: ''
      , surname: ''
    }

    this.handleChangeUniquifier = this.handleChangeUniquifier.bind(this)
  }

  handleChangeUniquifier(e) {
    const value = e.target.value
    if (value === '123456789') {
      this.setState(...this.state, { 
          uniquifier: value 
          , name: 'Janez'
          , surname: 'Novak'
        })
    } else {
      this.setState(...this.state, { 
        uniquifier: value 
        , name: ''
        , surname: ''
      })
    }

  }

  render() {
    return(
      <div className={style.patient}>
        <Grid>
          <TextField
              id="patient-uniquifier"
              label={this.props.uniquifierLabel}
              helperText={this.props.uniquifierHelperText}
              error={this.props.uniquifierError}           
              className={style.uniquifier}
              value={this.state.uniquifier}
              onChange={this.handleChangeUniquifier}
              disabled={false}
          >
            <Layout>
              <Desktop column="0" columnSpan="3"/>
              <Tablet column="0" columnSpan="3"/>
              <Phone/>
            </Layout> 
          </TextField>
          <TextField
              id="patient-name"
              label={this.props.nameLabel}         
              className={style.name}
              value={this.state.name}
              disabled={false}
          >
            <Layout>
              <Desktop column="3" columnSpan="3"/>
              <Tablet column="3" columnSpan="3"/>
              <Phone/>
            </Layout>
          </TextField>
          <TextField
              id="patient-surname"
              label={this.props.surnameLabel}         
              className={style.surname}
              value={this.state.surname}
              disabled={false}
          >
            <Layout>
              <Desktop column="6" columnSpan="6"/>
              <Tablet column="6" columnSpan="6"/>
              <Phone/>
            </Layout>
          </TextField>       
        </Grid>
      </div>
    )
  }
}

export default Patient