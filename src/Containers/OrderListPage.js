import React from 'react'

//Parila
import Grid from '../Parila/Grid'
import Layout from '../Parila/Layout'
import Desktop from '../Parila/Devices/Desktop'
import Tablet from '../Parila/Devices/Tablet'
import Phone from '../Parila/Devices/Phone'

//My components
import Customer from '../Components/Customer'
import OrderList from '../Components/OrderList'
import OrderListFooter from '../Components/OrderListFooter'

//Style
import style from './OrderListPage.css'

class OrderListPage extends React.Component {
  render() {
    return(
      <div className={style.orderListPage}>
        <Grid>
          <Customer>
            <Layout>
              <Desktop column="0" columnSpan="12"/>
              <Tablet column="0" columnSpan="12"/>
              <Phone/>
            </Layout>
          </Customer>
          <OrderList>
          </OrderList>
          <OrderListFooter orderListNewOrderButtonLabel='Novo naročilo'>
            <Layout>
              <Desktop column="0" columnSpan="12"/>
              <Tablet column="0" columnSpan="12"/>
              <Phone/>
            </Layout>            
          </OrderListFooter>
        </Grid>
      </div>
    )
  }
}

export default OrderListPage