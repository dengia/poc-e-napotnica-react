import React from 'react'

//Parila
import Grid from '../Parila/Grid'
import Layout from '../Parila/Layout'
import Desktop from '../Parila/Devices/Desktop'
import Tablet from '../Parila/Devices/Tablet'
import Phone from '../Parila/Devices/Phone'

//My components
import webApi from '../WebApi/WebApiFactory'
import Customer from '../Components/Customer'
import Patient from '../Components/Patient'
import Doctor from '../Components/Doctor'
import TissueList from '../Components/TissueList'
import Priority from '../Components/Priority'
import OrderFooter from '../Components/OrderFooter'
import InvisibleDiv from '../Components/InvisibleDiv'

//Style
import style from './OrderPage.css'
import styleRefferalDoctor from '../Components/RefferalDoctor.css'
import styleTissueDoctor from '../Components/TissueDoctor.css'

class OrderPage extends React.Component {
  constructor(props) {
    super(props)
    console.log(webApi.getTissueTemplateList())
    this.state = {
      tissues: webApi.getTissueTemplateList()
    }
  }

  render() {
    console.log('OrderPage.render')
    return(
      <div className={style.orderPage}>
        <Grid>
          <Customer>
            <Layout>
              <Desktop column="0" columnSpan="12"/>
              <Tablet column="0" columnSpan="12"/>
              <Phone/>
            </Layout> 
          </Customer>
          <Patient uniquifierLabel='KZZ' 
                    uniquifierHelperText='' 
                    uniquifierError={false}
                    nameLabel='Ime'
                    surnameLabel='Priimek'
          >
            <Layout>
              <Desktop column="0" columnSpan="8"/>
              <Tablet column="0" columnSpan="8"/>
              <Phone/>
            </Layout> 
          </Patient>
          <Priority>
            <Layout>
              <Desktop column="8" columnSpan="4"/>
              <Tablet column="8" columnSpan="4"/>
              <Phone/>
            </Layout> 
          </Priority>
          <InvisibleDiv>
            <Layout>
              <Desktop column="0" columnSpan="12"/>
              <Tablet column="0" columnSpan="12"/>
              <Phone/>
            </Layout> 
          </InvisibleDiv>
          <Doctor style={styleRefferalDoctor}>
            <Layout>
              <Desktop column="0" columnSpan="6"/>
              <Tablet column="0" columnSpan="6"/>
              <Phone/>
            </Layout>           
          </Doctor>
          <Doctor style={styleTissueDoctor}>
            <Layout>
              <Desktop column="6" columnSpan="6"/>
              <Tablet column="6" columnSpan="6"/>
              <Phone/>
            </Layout> 
          </Doctor>
          <InvisibleDiv>
            <Layout>
              <Desktop column="0" columnSpan="12"/>
              <Tablet column="0" columnSpan="12"/>
              <Phone/>
            </Layout> 
          </InvisibleDiv>          
          <TissueList tissues={this.state.tissues}>
            <Layout>
              <Desktop column="0" columnSpan="12"/>
              <Tablet column="0" columnSpan="12"/>
              <Phone/>
            </Layout> 
          </TissueList>
          <OrderFooter orderPrintButtonLabel='Tiskaj' orderOrderListButtonLabel='Seznam' orderSubmitButtonLabel='Pošlji'>
            <Layout>
              <Desktop column="0" columnSpan="12"/>
              <Tablet column="0" columnSpan="12"/>
              <Phone/>
            </Layout>            
          </OrderFooter>
        </Grid>
      </div>
    )
  }
}

export default OrderPage