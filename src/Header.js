import React from 'react'

//Material UI
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class Header extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <div>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Inštitut za patologijo - naročanje
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default Header