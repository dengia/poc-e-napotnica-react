import React from 'react'
import { Switch, Route } from 'react-router-dom'

//My Components
import OrderPage from './Containers/OrderPage'
import OrderListPage from './Containers/OrderListPage'
import OrderPrintPage from './Containers/OrderPrintPage'

class Content extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    console.log('Content.render')
    return(
      <main>
        <Switch>
          <Route exact path='/' component={OrderListPage}/>
          <Route path='/order' component={OrderPage}/>
          <Route path='/order-print' component={OrderPrintPage}/>
        </Switch>
      </main>
    )
  }
}

export default Content