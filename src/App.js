import React from 'react'
import Header from './Header'
import Content from './Content'
import style from './App.css'

class App extends React.Component {
  render(){
  return(
    <div style={style}>
      <Header />
      <Content />
    </div>
  )
  }
}
export default App