const express = require('express')
const webpack = require('webpack')
const webpackMiddleware = require('webpack-dev-middleware')
const config = require('./webpack.dev.js')

const app = express()
const compiler = webpack(config)

app.use(webpackMiddleware(compiler, {
  publicPath: config.output.publicPath
}))

app.listen(3000, () => {
  console.log('e-napotnica listening on port 3000\n')
})